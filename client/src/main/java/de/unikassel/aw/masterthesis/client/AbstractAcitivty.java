package de.unikassel.aw.masterthesis.client;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

public abstract class AbstractAcitivty extends Activity {
	
	protected static final int TIMEOUT = 3000;
	public static final String PREFS_NAME = "mmlf";
	protected SharedPreferences settings;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		 // We need an Editor object to make preference changes.
	      // All objects are from android.context.Context
	      settings = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
	      int i=0; i++;
	     

	      
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.menu, menu);
	    return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle item selection
		Intent i;
	    switch (item.getItemId()) {
		    case R.id.exitItem:
		        finish();
		        return true;
		    case R.id.optionsItem:
		    	if (this.getClass().equals(OptionsActivity.class)) {
		    		return true;
		    	}
		    	i = new Intent(getApplicationContext(), OptionsActivity.class);
		    	startActivity(i);
		        return true;
		    case R.id.mainItem:
		    	if (this.getClass().equals(MainActivity.class)) {
		    		return true;
		    	}
		    	i = new Intent(getApplicationContext(), MainActivity.class);
		    	startActivity(i);
		    	return true;
		    default:
		        return super.onOptionsItemSelected(item);
		    }
	}

}
