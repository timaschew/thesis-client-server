package de.unikassel.aw.masterthesis.client;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class ProfileActivity extends AbstractAcitivty {
	
	private Intent intent;
	protected String delay;

	public String getProfileName() {
		String profileName = intent.getStringExtra("profileName");
		return profileName;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		final Context context = getApplicationContext();
		setContentView(R.layout.profile);
		
		intent = getIntent();
		String profileName = getProfileName();
		
		TextView profileNameText = (TextView) findViewById(R.id.profileNameText);
		profileNameText.setText(profileName);
		
		
		
		final ArrayList<String> sensorList = new ArrayList<String>();
		ServiceHelper sh = new ServiceHelper(settings);
		try {
			JSONObject result = sh.findProfileByName(profileName);
			
			final ListView sensorListView = (ListView) findViewById(R.id.sensorList);
			final ArrayAdapter<String> sensorListAdapter = new ArrayAdapter<String>(this,
					android.R.layout.simple_list_item_1, new ArrayList<String>());
			sensorListView.setAdapter(sensorListAdapter);
			
			TextView descriptionText = (TextView) findViewById(R.id.descriptionText);
			descriptionText.setText(result.getString("description"));
			TextView learnTypeText = (TextView) findViewById(R.id.learnTypeText);
			learnTypeText.setText(result.getString("learnType"));
			
			TextView samplingText = (TextView) findViewById(R.id.samplingText);
			delay = result.getString("delay");
			samplingText.setText(delay);
			
			TextView preprocessingText = (TextView) findViewById(R.id.preprocessingText);
			String preprocessingName = result.getJSONObject("preprocessing").getString("name");
			preprocessingText.setText(preprocessingName);
						
			JSONArray sensors = result.getJSONArray("sensor");
			for (int i = 0; i < sensors.length(); i++) {
				String element = sensors.getJSONObject(i).getString("name");
				sensorListAdapter.add(element);
				sensorList.add(element);
			}
			
		} catch (ClientProtocolException e) {
			Log.e("bla", e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			Log.e("bla", e.getMessage());
			e.printStackTrace();
		} catch (JSONException e) {
			Log.e("bla", e.getMessage());
			e.printStackTrace();
		}
		
		final Button button = (Button) findViewById(R.id.recordButton);
		button.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				if (sensorList.isEmpty() == false) {
					Intent i = new Intent(context, RecordActivity.class);
					i.putStringArrayListExtra("sensors", sensorList);
					i.putExtra("profileName", getProfileName());
					i.putExtra("delay", delay);
					startActivity(i);
				}
				
			}
		});
		
		
	}
	
}
