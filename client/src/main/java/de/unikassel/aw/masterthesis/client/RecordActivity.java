package de.unikassel.aw.masterthesis.client;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import de.unikassel.aw.masterthesis.client.RecordService.LocalBinder;

public class RecordActivity extends AbstractAcitivty  {//implements SensorEventListener {
	
	private TextView serviceStatus;
	private ServiceHelper sh;
	private String profileName;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Thread ct = Thread.currentThread();
		Log.i(MMLF.T, "creating Record Activity by thread: "+ct.getId()+ "("+ct.getName()+")");
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.record);
		serviceStatus = (TextView) findViewById(R.id.serviceStatus);
		sh = new ServiceHelper(settings);

		
		Intent intent = getIntent();
		profileName = intent.getStringExtra("profileName");
				
		final ArrayList<String> sensors = intent.getStringArrayListExtra("sensors");
		final String samplingRate = intent.getStringExtra("delay");
		
		
		final Button startButton = (Button) findViewById(R.id.startButton);
		startButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
//				
				mService.startRecord(profileName, sensors, samplingRate);
				serviceStatus.setText("running...");
			}
		});
		
		final Button stopButton = (Button) findViewById(R.id.stopButton);
		stopButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				mService.stopRecord();
				serviceStatus.setText("stopped");
			}
		});
		
		final TextView sessionHash = (TextView) findViewById(R.id.sessionHashText);
		
		Button annotationButon = (Button) findViewById(R.id.annotationButton);
		annotationButon.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				try {
					JSONObject session = sh.createSession(profileName);
					String hash = session.getString("annotationsesssion");
					String sessionID = session.getString("id");
					sessionHash.setText(hash);
					mService.setAnnotation(sessionID);
				} catch (Exception e) {
					Log.e(MMLF.T, e.getMessage(), e);
				} 
			}
		});
	}
	
	@Override
	protected void onStart() {
		super.onStart();
        if (mBound == false) {
        	/*
        	 * so geht das nicht, muss über broad cast receiver erfolgen
        	 * 
        	Intent serviceIntent = new Intent(this, RecordService.class);
        	startService(serviceIntent);
        	 */
        	Intent serviceIntent = new Intent(this, RecordService.class);
            bindService(serviceIntent, mConnection, Context.BIND_AUTO_CREATE);
            Log.i(MMLF.T, "bound activity to service");
        }
	}
	
	@Override
	protected void onStop() {
		super.onStop();
		if (mBound) {
			unbindService(mConnection);
			mBound = false;
		}
		Log.i(MMLF.T, "unbound service");
	};
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		Log.i(MMLF.T, "destroy record activity");
	};
	
	RecordService mService;
	boolean mBound = false;
	
    /** Defines callbacks for service binding, passed to bindService() */
    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            LocalBinder binder = (LocalBinder) service;
            mService = binder.getService();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };
	


}
