package de.unikassel.aw.masterthesis.client;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Binder;
import android.os.Environment;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

public class RecordService extends Service implements SensorEventListener {

	private final IBinder iBinder = new LocalBinder();

	private String sessionAnnotationID = null;
	
	private static RecordService instance;
	
	private static File sensorFile;
	private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSZ");
	private static SimpleDateFormat fileFormat = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
	
	private static long firsttime;
	private static long lastUpdate = 0L;
	private static int dataCounter = 0;

	private static SharedPreferences settings;

	private static SensorManager sensorManager;

	private static String profileName;
	
	@Override
	public void onCreate() {
		super.onCreate();
		Log.i(MMLF.T, "record service created");
		
		settings = getSharedPreferences(AbstractAcitivty.PREFS_NAME, Context.MODE_PRIVATE);
		sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
	}
	@Override
	public void onStart(Intent intent, int startId) {
		Log.i(MMLF.T, "record service started");
		super.onStart(intent, startId);
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		Log.i(MMLF.T, "on start called");
		return START_STICKY;
	}
	
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		stopRecord();
		Log.i(MMLF.T, "record service destroyed");
	}
	
	@Override
	public IBinder onBind(Intent intent) {
		return iBinder;
	}
	
	public class LocalBinder extends Binder {
		RecordService getService() {
			return RecordService.this;
		}
	}
	
	public void startRecord(String profileName, ArrayList<String> sensors, String samplingRate) {
		if (instance == null) {
			instance = this;
			Log.i(MMLF.T, "start recotding...");
			checkFilePermissions(profileName);
			RecordService.profileName = profileName;
			registerSensors(sensors, samplingRate);
		} else {
			Log.i(MMLF.T, "already recording");
		}
		
	}

	public void stopRecord() {
		if (instance != null) {
			instance = null;
			sensorManager.unregisterListener(this);
			Log.i(MMLF.T, "stop recotding");
			long diffMillis = lastUpdate - firsttime;
			long seconds = diffMillis / 1000;
			Log.i(MMLF.T, "count "+dataCounter+" in "+seconds+" sec.");
			try {
				long avg = dataCounter / seconds;
				Log.i(MMLF.T, "Average: "+avg+ " data per seconds");
			} catch (Exception e) {
				Log.e(MMLF.T, e.getMessage(), e);
			}
			
			sendToServer();
		} else {
			Log.i(MMLF.T, "nothing to stop");
		}
	}
	
	private void registerSensors(ArrayList<String> sensors, String samplingRate) {
		SensorManager sm = (SensorManager) getSystemService(SENSOR_SERVICE);
		List<Sensor> list = sm.getSensorList(Sensor.TYPE_ALL);
		
		Integer androidDelay = SensorHelper.delayEnumMap.get(samplingRate);
		if (androidDelay == null) {
			Log.i(MMLF.T, "could not load samplingrate from enum "+samplingRate+ "use default: normal");
			androidDelay = SensorManager.SENSOR_DELAY_NORMAL;
		}
		for (Sensor s : list) {
			
			String sensorName = SensorHelper.sensorIntMap.get(s.getType());
			if (sensors.contains(sensorName)) { 
				sensorManager.registerListener(this, s,	androidDelay);
				Log.i(MMLF.T, "register sensor: "+sensorName+ " with delay: "+androidDelay+" / "+SensorHelper.delayIntMap.get(androidDelay));
			}
		}
	}
	

	
	private void checkFilePermissions(String profileName) {
		String state = Environment.getExternalStorageState();
		if (Environment.MEDIA_MOUNTED.equals(state)) {
			File directory = new File(Environment.getExternalStorageDirectory() + "/mmlf");
			if (directory.exists()) {
				sensorFile = new File(directory, fileFormat.format(new Date())+"_"+profileName+".csv");
				Log.i(MMLF.T, "created file: "+sensorFile);
			} else {
				Log.i(MMLF.T, "could not created file: "+sensorFile);
			}
		} else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
			Log.i(MMLF.T, "ExternalStorageState has wrong state (read-only): "+state);
		} else {
			Log.i(MMLF.T, "ExternalStorageState has wrong state: "+state);
		}
		lastUpdate = System.currentTimeMillis();
		firsttime = System.currentTimeMillis();
		Log.i(MMLF.T, "start to record sensor data");
		
	}
	
	
	private void sendToServer() {
		final ServiceHelper sh = new ServiceHelper(settings);
		String retVal = sh.uploadFile(sensorFile, profileName, sessionAnnotationID);
		if (retVal != null) {
			Toast.makeText(getApplicationContext(), "File upload was successfull", Toast.LENGTH_LONG).show();
			Toast.makeText(getApplicationContext(), "DataContainer ID: "+retVal, Toast.LENGTH_LONG).show();
			
			/*
			  	TODO: call asynch. the result of the datacontainer and display
			*/
			JSONObject result;
			try {
				Thread.sleep(1000);
				result = sh.findDataContainer(retVal);
				Log.i(MMLF.T, result.toString());
				String containerResult = result.getString("result");
				Toast.makeText(getApplicationContext(), "Result: "+containerResult, Toast.LENGTH_LONG).show();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				Log.e(MMLF.T, e.getMessage(), e);
			} 
			
			
		} else {
			Toast.makeText(getApplicationContext(), "Could not upload file to server!", Toast.LENGTH_LONG).show();
		}
	}
	
	private final static char DELIMITER =  ';';
	private final static char NEWLINE =  '\n';
	
	
	public void onSensorChanged(SensorEvent event) {
		if (instance == null) {
			return;
		}
		
		long actualTime = System.currentTimeMillis();
//		 diff = actualTime  - lastUpdate;
		String hardwareName = event.sensor.getName();
		String sensorType = SensorHelper.sensorIntMap.get(event.sensor.getType());
				
		float[] values = event.values;
		
		try { 
                      
			FileWriter fw = new FileWriter(sensorFile, true);
//            FileOutputStream fOut = new FileOutputStream(sensorFile);
//            OutputStreamWriter osw = new OutputStreamWriter(fOut); 

            StringBuilder sb = new StringBuilder();
            
            // Write the string to the file
//            sb.append(sdf.format(new Date())).append(DELIMITER);
            sb.append(System.currentTimeMillis()).append(DELIMITER);
            sb.append(sensorType).append("[").append(hardwareName).append("]").append(DELIMITER);
//            sb.append(hardwareName).append(DELIMITER);
            for (int i=0; i<values.length-1; i++) {
            	sb.append(values[i]).append(DELIMITER);
            }
            sb.append(values[values.length-1]).append(NEWLINE);
            fw.write(sb.toString());
            fw.flush();
            fw.close();
            dataCounter++;
		} catch (Exception e) {
			Log.e("error", e.getMessage(), e);
		}
		
		lastUpdate = actualTime;
	}

	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		Log.d(MMLF.T, "accuracy has changed by sensor: "+sensor.getName());
	}
	public void setAnnotation(String value) {
		sessionAnnotationID = value;
	}

}
