package de.unikassel.aw.masterthesis.client;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AbstractAcitivty {

	private ServiceHelper sh;
	private ArrayAdapter<String> adapter;
	private Context context;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		Log.i(MMLF.T, "Main app created");
		super.onCreate(savedInstanceState);
		context = getApplicationContext();
		setContentView(R.layout.main);
		sh = new ServiceHelper(settings);

		final ListView listView = (ListView) findViewById(R.id.profileList);
		adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, new ArrayList<String>());
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				String profileNAme = (String) listView.getItemAtPosition(position);
				Log.d(MMLF.T, "select: " + profileNAme);
				Intent i = new Intent(context, ProfileActivity.class);
				i.putExtra("profileName", profileNAme);
				startActivity(i);
			}
		});
		final Button button = (Button) findViewById(R.id.updateProfilesButton);
		button.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				updateProfiles();
			}
		});
		
		updateProfiles();

	}

	private void updateProfiles() {
		try {
			JSONArray array = sh.findAllProfiles();
			adapter.clear();
			for (int i = 0; i < array.length(); i++) {
				String element = array.getJSONObject(i).getString("name");
				adapter.add(element);
			}
			if (array.length() == 0) {
				Toast.makeText(getApplicationContext(), "No profiles found on server", Toast.LENGTH_LONG).show();
			}
		} catch (JSONException e) {
			Log.e(MMLF.T, e.getMessage(), e);
		} catch (ClientProtocolException e) {
			Log.e(MMLF.T, e.getMessage(), e);
		} catch (IOException e) {
			Toast.makeText(context,	e.getMessage(), Toast.LENGTH_LONG).show();
			Log.e(MMLF.T, e.getMessage(), e);
		}
		
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

}