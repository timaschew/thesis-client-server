package de.unikassel.aw.masterthesis.client;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.DuplicateFormatFlagsException;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

public class OptionsActivity extends AbstractAcitivty {


	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		final Context context = getApplicationContext();
		setContentView(R.layout.options);
		final ServiceHelper sh = new ServiceHelper(settings);

		
		final EditText serverUrlField = (EditText) findViewById(R.id.servernUrl);
		serverUrlField.setText(settings.getString("serverUrl", Settings.serverUrl));
		final CheckBox serverConnectionCB =  (CheckBox) findViewById(R.id.connectionCheckbox);
		serverConnectionCB.setChecked(false);
		serverUrlField.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				Editor edit = settings.edit();
				edit.putString("serverUrl", s.toString());
				edit.commit();
				Log.i(MMLF.T, "commited: "+s.toString());
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,int after) {}
			
			@Override
			public void afterTextChanged(Editable s) {}
		});


		final Button button = (Button) findViewById(R.id.testConnection);
		button.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				serverConnectionCB.setChecked(false);
				boolean connectionOK = sh.testConnection();

				if (connectionOK) {
					serverConnectionCB.setChecked(true);
					Toast.makeText(context, "Connection established", Toast.LENGTH_LONG).show();
					return;
				} else {
					Toast.makeText(context, "Could not connect", Toast.LENGTH_LONG).show();
				}
			}
		});
		
		final EditText userNameField = (EditText) findViewById(R.id.userNameText);
		userNameField.setText(settings.getString("username", Settings.username));
		userNameField.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				Editor edit = settings.edit();
				edit.putString("username", s.toString());
				edit.commit();
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,int after) {}
			
			@Override
			public void afterTextChanged(Editable s) {}
		});
		
		final EditText passwordField = (EditText) findViewById(R.id.passwordText);
		passwordField.setText(settings.getString("password", Settings.password));
		passwordField.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				Editor edit = settings.edit();
				edit.putString("password", s.toString());
				edit.commit();
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,int after) {}
			
			@Override
			public void afterTextChanged(Editable s) {}
		});

		final CheckBox loginCheckBox =  (CheckBox) findViewById(R.id.loginCheckbox);
		loginCheckBox.setChecked(false);
		final Button loginButton = (Button) findViewById(R.id.checkLoginButton);
		loginButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				loginCheckBox.setChecked(false);
				boolean connectionOK = false;
				try {
					connectionOK = sh.testLogin(userNameField.getText().toString(), passwordField.getText().toString());
				} catch (Exception e) {
					Log.e(MMLF.T, e.getMessage(), e);
				} 
				if (connectionOK) {
					loginCheckBox.setChecked(true);
					Toast.makeText(context, "Authentification successfull", Toast.LENGTH_LONG).show();
					return;
				} else {
					Toast.makeText(context, "Wrong user/pass", Toast.LENGTH_LONG).show();
				}
			}
		});
	}
}
