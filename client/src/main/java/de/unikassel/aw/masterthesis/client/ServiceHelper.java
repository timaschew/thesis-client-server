package de.unikassel.aw.masterthesis.client;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.MessageDigest;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.HttpVersion;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

public class ServiceHelper {

	private static final int TIMEOUT = 2000;
	private SharedPreferences sharedPref;

	public ServiceHelper(SharedPreferences sharedPref) {
		this.sharedPref = sharedPref;
	}

	public String find(final String pathUrl) throws ClientProtocolException, IOException, JSONException {

		String serverUrl = sharedPref.getString("serverUrl", Settings.serverUrl);
		Log.d(MMLF.T, "load "+serverUrl+" from shared prefs");
		String url = serverUrl.concat(pathUrl);
		Log.i(MMLF.T, "url: " + url);
		HttpParams httpParameters = new BasicHttpParams();
		// Set the timeout in milliseconds until a connection is established.
		HttpConnectionParams.setConnectionTimeout(httpParameters, TIMEOUT);
		HttpClient hc = new DefaultHttpClient(httpParameters);

//		HttpPost post = new HttpPost(url);
		 HttpGet get = new HttpGet(url);

		HttpResponse rp = hc.execute(get);
		if (rp.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
			String str = EntityUtils.toString(rp.getEntity());
			Log.d(MMLF.T, "result: " + str);
			return str;
		}
		
		return null;
	}


	public JSONObject createSession(String profileName) throws ClientProtocolException, UnsupportedEncodingException, IOException, JSONException {
		String username = sharedPref.getString("username", Settings.username);
		String retVal = find(Settings.pathSessions + "create?profile="+URLEncoder.encode(profileName, "utf-8")+"&username="+URLEncoder.encode(username, "utf-8"));
		return new JSONObject(retVal);
	}

	public JSONObject findDataContainer(String id) throws ClientProtocolException, UnsupportedEncodingException, IOException, JSONException {
		String retVal = find(Settings.pathDataContainers + "id?id="+URLEncoder.encode(id, "utf-8"));
		return new JSONObject(retVal);
	}
	
	public JSONArray findAllProfiles() throws ClientProtocolException, IOException, JSONException {
		String retVal = find(Settings.pathProfiles + "allAsJSON");
		return new JSONArray(retVal);
	}

	public JSONObject findProfileByName(final String name) throws ClientProtocolException, IOException, JSONException {
		String retVal = find(Settings.pathProfiles + "name?name="+ URLEncoder.encode(name, "utf-8"));
		return new JSONObject(retVal);
	}
	
	public boolean testLogin(String username, String password) throws ClientProtocolException, UnsupportedEncodingException, IOException, JSONException {
		String retVal = find(Settings.pathUsers+"checkPassForUser?username="+URLEncoder.encode(username, "utf-8")+"&password="+URLEncoder.encode(password, "utf-8"));
		if (retVal.equals("true")) {
			return true;
		}
		return false;
	}

	public boolean testConnection() {
		String str = "";
		try {
			str = find(Settings.pathConnection);
		} catch (ClientProtocolException e) {
			Log.e(MMLF.T, e.getMessage(), e);
		} catch (IOException e) {
			Log.e(MMLF.T, e.getMessage(), e);
		} catch (JSONException e) {
			Log.e(MMLF.T, e.getMessage(), e);
		}
		if (str.equals("world")) {
			return true;
		}
		return false;
		
	}

	public String uploadFile(File f, String profileName, String annotationSessionID) {
		String success = null;
		try {
		    DefaultHttpClient httpclient = new DefaultHttpClient();
		    
			String serverUrl = sharedPref.getString("serverUrl", Settings.serverUrl);
			Log.d(MMLF.T, "load "+serverUrl+" from shared prefs");
			String fileUpload = sharedPref.getString("fileUpload", Settings.fileUpload);

			String username = sharedPref.getString("username", Settings.username);
			String password = sharedPref.getString("password", Settings.password);
			
			String url = serverUrl.concat(fileUpload);
			Log.i(MMLF.T, "url: " + url);
			
		    HttpPost httpost = new HttpPost(url);
		    MultipartEntity entity = new MultipartEntity();
		    entity.addPart("username", new StringBody(username));
		    entity.addPart("password", new StringBody(password));
		    entity.addPart("profileName", new StringBody(profileName));
		    if (annotationSessionID == null) {
		    	annotationSessionID = "-1";
		    }
		    entity.addPart("annotationID", new StringBody(annotationSessionID));
		    entity.addPart("file", new FileBody(f));
		    httpost.setEntity(entity);

		    HttpResponse response;
		    response = httpclient.execute(httpost);

		    Log.d(MMLF.T, "file upload response status: " + response.getStatusLine());
		    
		    if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				String str = EntityUtils.toString(response.getEntity());
				success = str;
			}
		} catch (Exception ex) {
		    Log.d(MMLF.T, "Upload failed: " + ex.getMessage() +
		        " Stacktrace: " + ex.getStackTrace());
		    
		} 
		return success;

		
	}




	
}
