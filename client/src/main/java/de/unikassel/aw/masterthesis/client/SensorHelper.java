package de.unikassel.aw.masterthesis.client;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.util.Log;

/**
 * This framework use an enum for the sensors but android use integer values. Need a bidirectional map for it.<br>
 * The same thing for the sensor sampling rate / delay.
 * 
 * @author awilhelm
 */
public class SensorHelper {
	
	/**
	 * Sensorname
	 * (F) Enum -> (A) Integer
	 */
	public static Map<String, Integer> sensorEnumMap = new HashMap<String, Integer>();
	
	/**
	 * Sensorname
	 * (A) Integer -> (F) Enum
	 */
	public static Map<Integer, String> sensorIntMap = new HashMap<Integer, String>();
	
	
	/**
	 * Sampling rate / delay
	 * (F) Enum -> (A) Integer
	 */
	public static Map<String, Integer> delayEnumMap = new HashMap<String, Integer>();
	
	/**
	 * Sampling rate / delay
	 * (A) Integer -> (F) Enum
	 */
	public static Map<Integer, String> delayIntMap = new HashMap<Integer, String>();
	
	public static boolean ready = initialize();
	
	private static boolean initialize() {
		// initialize sensor enum-integer maps
		{
			Class<Sensor> clazz = Sensor.class;
			Field[] fields = clazz.getDeclaredFields();
			for (Field f : fields) {
				try {
					String fieldName = f.getName();
					if (fieldName.startsWith("TYPE_") == false) {
						continue;
					}
					int value = f.getInt(null);
					sensorEnumMap.put(fieldName, value);
					sensorIntMap.put(value, fieldName);
				} catch (IllegalArgumentException e) {
					Log.e(MMLF.T, e.getMessage(), e);
					return false;
				} catch (IllegalAccessException e) {
					Log.e(MMLF.T, e.getMessage(), e);
					return false;
				}
			}
		}
		
		// initialize sampling rate enum-integer maps
		{
			Class<SensorManager> clazz = SensorManager.class;
			Field[] fields = clazz.getDeclaredFields();
			for (Field f : fields) {
				try { // if name starts with "TYPE_"
					String fieldName = f.getName();
					if (fieldName.startsWith("SENSOR_DELAY_") == false) {
						continue;
					}
					int value = f.getInt(null);
					delayEnumMap.put(fieldName, value);
					delayIntMap.put(value, fieldName);
				} catch (IllegalArgumentException e) {
					Log.e(MMLF.T, e.getMessage(), e);
					return false;
				} catch (IllegalAccessException e) {
					Log.e(MMLF.T, e.getMessage(), e);
					return false;
				}
			}
		}
		return true;
	}

}
