# Development requirements:
* JDK 1.6
* Playframework 1.2.3 http://www.playframework.org/documentation/1.2.3/install 
* Git http://git-scm.com/ (dont forget to set
* Android sdk,2.3.3 or API version 10 http://developer.android.com/sdk/index.html
* Eclipse http://www.eclipse.org/downloads/
* Eclipse plugins
* * maven integration for eclipse http://www.eclipse.org/m2e/
* * git integration for eclipse http://www.eclipse.org/egit/
* * android integration for eclipse: ADT http://developer.android.com/sdk/eclipse-adt.html
* * maven for android integration http://code.google.com/p/maven-android-plugin/wiki/EclipseIntegration

# Configuration
## Git repository
in eclipse git repository perspective clone the git repository with the current url  
``https://bitbucket.org/thunderwave/thesis-client-server``  
for write access type in your login+password or public key with ssh  

alternative clone from console and add existing local repository to eclipse repository view  
clone git repository to file system  
``git clone https://bitbucket.org/thunderwave/thesis-client-server.git``  
add local git repository in eclipse git repository perspective  

## Checkout projects
then checkout from working directory the two projects:  
client with "Import as maven projects..." (if you cannot import as maven, then import as general project, then convert into a maven project and then right click on project -> Maven -> Update project configuration)  
server with "Import project..." -> choose "Import as general project" use project name "thesis-server"  
open console (not! cygwin, because the path would be wrong with /cygdrive/c)  
and go to thesis-server and run command  
``play eclipsify``  
then refresh / clean the server project  
to start the srever, you can use the launcher in the eclipse dir  
for debugging, first start server, then start launcher "Connect JPDA to ... "  

## Framework configuration
### Server
* conf/application.conf: set upload path and database setting
* when start the server, open http://localhost:9000/admin in your browser
* create a user and profile with the required fileds

### Client
* open Settings.java and set the serverUrl and user+password values