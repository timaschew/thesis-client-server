package preproccessing;
import impl.preprocessing.MyCustomPreprocessing;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;


import org.junit.Test;

import play.test.UnitTest;
import utils.CsvHelper;
import utils.Data;


public class MyCustomPreprocessingTest extends UnitTest {

    @Test
    public void test() throws IOException {

    	String fileName = this.getClass().getResource("accSensor.csv").getFile();
//    	InputStreamReader converter = new InputStreamReader(new FileInputStream(fileName));
//		BufferedReader in = new BufferedReader(converter);
//    	System.out.println(in.readLine());
    	
    	ArrayList<Data> dataList = CsvHelper.parseCsv(fileName);
    	
    	MyCustomPreprocessing p = new MyCustomPreprocessing();
    	p.process(dataList);
    	
    	
    }

}
