package utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import controllers.Application;

import play.Logger;


public class CsvHelper {
	
	public static final String DELIMITER = ";";
	public static final String NEW_LINE = "\n";
	private static SimpleDateFormat fileFormat = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
	private static final String EXTENSION = ".csv";

	
	public static final Integer COL_TIMESTAMP = 0;
	public static final Integer COL_DESCRIPTION = 1;
	public static final Integer COL_OFFSET_VALUES = 2;


	public static ArrayList<Data> parseAnnotationCsv(String fileName) {
		ArrayList<Data> list = new ArrayList<Data>();
		FileReader fr;
		try {
			
			fr = new FileReader(fileName);
			BufferedReader br = new BufferedReader(fr);
			String line = null;
			while ((line = br.readLine()) != null) {
				String[] cols = line.split(DELIMITER);
				Data d = new Data();
				d.timestamp = Long.parseLong(cols[COL_TIMESTAMP]);
				d.meta = cols[COL_DESCRIPTION];
				list.add(d);
			}
			
		} catch (FileNotFoundException e) {
			Logger.error(e, e.getMessage());
		} catch (IOException e) {
			Logger.error(e, e.getMessage());
		}
		return list;
		
	}
	
	public static ArrayList<Data> parseCsv(String fileName) {
		ArrayList<Data> list = new ArrayList<Data>();
		FileReader fr;
		try {
			
			fr = new FileReader(fileName);
			BufferedReader br = new BufferedReader(fr);
			String line = null;
			while ((line = br.readLine()) != null) {
				String[] cols = line.split(DELIMITER);
				Data d = new Data();
				d.timestamp = Long.parseLong(cols[COL_TIMESTAMP]);
				d.name = cols[COL_DESCRIPTION];
				d.values = new String[cols.length-COL_OFFSET_VALUES];
				for (int i=COL_OFFSET_VALUES; i<cols.length; i++) {
					int localIndex = i - COL_OFFSET_VALUES;
					d.values[localIndex] = cols[i];
				}
				list.add(d);
			}
			
		} catch (FileNotFoundException e) {
			Logger.error(e, e.getMessage());
		} catch (IOException e) {
			Logger.error(e, e.getMessage());
		}
		return list;
		
	}


	public static void write(ArrayList<Data> resultList, File file, boolean append) {
		StringBuilder sb = new StringBuilder();
		for (Data d : resultList) {
			sb.append(d.timestamp).append(DELIMITER);
			sb.append(d.name).append(DELIMITER);
			String[] values = d.values;
			sb.append(StringUtils.join(values, DELIMITER));
			if  (d.meta != null) {
				sb.append(DELIMITER).append(d.meta);
			}
			sb.append(NEW_LINE);
		}
//		String fileName = fileFormat.format(new Date());
		try {
			FileWriter fw = new FileWriter(file, append);
			fw.write(sb.toString());
			fw.flush();
			fw.close();
			Logger.info("write condensed file to: %s", file.getAbsoluteFile());
		} catch (IOException e1) {
			Logger.error(e1, e1.getMessage());
		}
		
	}

}
