package utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

import org.apache.log4j.Logger;

public class DataHelper {
	
	private final static Logger log = Logger.getLogger(DataHelper.class);

	private static final String EMPTY_STRING = "";

	public static ArrayList<ArrayList<Data>> createPseudoInputVectors(ArrayList<Data> resultList, String[] nameVector) {
		
		ArrayList<ArrayList<Data>> listOfVectors = new ArrayList<ArrayList<Data>>();
		
		Map<String, Data> vectorSet = new HashMap<String, Data>();
		for (Data d : resultList) {
			// sensor type, without the hardware name enclosed in []
			String realKey = d.name.replaceFirst("\\[.*\\]", "");
			d.name = realKey;
			if (vectorSet.keySet().size() == nameVector.length) {
				// set is complete
				ArrayList<Data> vector = buildVector(vectorSet, nameVector);
				listOfVectors.add(vector);
				vectorSet.clear();
			} else {
				// collect more elements (sensor types)
				vectorSet.put(realKey, d);
			}
		}
		return listOfVectors;
		
	}
	
	public static ArrayList<ArrayList<RealData>> createInputVectors(ArrayList<Data> dataVectors, String[] nameVector) {
		
		ArrayList<ArrayList<Data>> pseudoVectors = createPseudoInputVectors(dataVectors, nameVector);
		
		ArrayList<ArrayList<RealData>> realDataVectors = new ArrayList<ArrayList<RealData>>();
		
		vector: for (ArrayList<Data> vector : pseudoVectors) {
			ArrayList<RealData> realVector = new ArrayList<RealData>();
			for (Data field : vector) {
				for (int i=0; i<field.values.length; i++) {
					String value = field.values[i];
					RealData realData = new RealData(field);
					if (value.equals(EMPTY_STRING) == false) {
						realData.name = field.name+"{"+i+"}";
						realData.value = value;
						realVector.add(realData);
					} else {
						// remove complete vector (or fill with neutral values?)
						continue vector;
					}
					
				}
			}
			
			realDataVectors.add(realVector);
		}
		return realDataVectors;
		
		
	}

	public static ArrayList<Data> buildVector(Map<String, Data> vectorSet, String[] nameVector) {
		ArrayList<Data> vector = new ArrayList<Data>();
		SortedSet<Long> timeStamps = new TreeSet<Long>();
		for (String sensorType : nameVector) {
			Data data = vectorSet.get(sensorType);
			vector.add(data);
			if (data == null) {
				System.err.println("OOHHHH");
			}
			timeStamps.add(data.timestamp);
		}
		
		log.debug(String.format("first timestamp= %d last= %d", timeStamps.first(), timeStamps.last()));
		Long refTimeStamp = (Long) timeStamps.toArray()[timeStamps.size()/2];
		log.debug(String.format("using timestamp %d as ref", refTimeStamp));
		
		// TODO: need own structure / class ?
		// modifying all timestamps 
		for (String sensorType : nameVector) {
			Data data = vectorSet.get(sensorType);
			data.timestamp = refTimeStamp;
		}
		
		return vector;
	}


}
