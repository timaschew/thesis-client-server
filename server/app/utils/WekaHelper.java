package utils;

import java.util.ArrayList;

import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;


public class WekaHelper {

	private static final double WEIGHT = 1.0;

	public static Instances createArffFile(ArrayList<ArrayList<RealData>> listOfVectors, String name) {
				
		FastVector attributeVector = new FastVector();
		for (RealData field : listOfVectors.get(0)) {
			attributeVector.addElement(new Attribute(field.name));
		}
		
		Instances data = new Instances(name, attributeVector, 0);
		
		for (ArrayList<RealData> vector : listOfVectors) {
			
			double[] instanceValues = new double[data.numAttributes()];
			for (int i=0; i<vector.size(); i++) {
				RealData field = vector.get(i);
				instanceValues[i] = Double.parseDouble(field.value);
				
			}
			data.add(new Instance(WEIGHT, instanceValues));
			
		}
		
		return data;
		
	}

}
