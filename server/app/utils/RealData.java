package utils;

public class RealData {

	public RealData(Data field) {
		timestamp = field.timestamp;
		meta = field.meta;
	}

	public long timestamp;
	
	public String name;
	
	public String meta;
	
	public String value;
	
	@Override
	public String toString() {
		return name+"|"+meta+"|"+value;
	}
	
}
