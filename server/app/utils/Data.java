package utils;

import java.io.Serializable;

public class Data {
	
	public long timestamp;
	
	public String name;
	
	public String meta;
	
	public String[] values;
	
	@Override
	public String toString() {
		return name+"|"+meta+"|"+values;
	}
	
}
