package utils;

import impl.AbstractPreprocessingImpl;
import impl.PreprocessingInterface;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import org.apache.log4j.Logger;

import models.Preprocessing;

public class ApplicationHelper {

	private static final Logger log = Logger.getLogger(ApplicationHelper.class);

	public static void loadPreprocessingImplementations() {
		try {

			List<Class> classes = getClasses("impl.preprocessing");
			for (Class clazz : classes) {
				log.info(String.format("found class: %s", clazz.getName()));
				// TODO: check interface
				if (clazz.getSuperclass().equals(AbstractPreprocessingImpl.class)) {
					// ok
				} else {
					log.warn(String.format("found classes in impl.preprocssing, which are not instance of AbstractPreprocessingImpl: %s", clazz.getName()));
					continue;
				}
				AbstractPreprocessingImpl ppImpl = (AbstractPreprocessingImpl) clazz.newInstance();
				ppImpl.doBefore();
				if (Preprocessing.find("byName", ppImpl.preprocessing.name).fetch().isEmpty()) {
					ppImpl.preprocessing.save();
					log.info(String.format("class: %s loaded successfully", clazz.getName()));
				} else {
					log.info(String.format("class: %s is already in database ", clazz.getName()));

				}
				
			}
		} catch (ClassNotFoundException e) {
			log.error(e);
			e.printStackTrace();
		} catch (IOException e) {
			log.error(e.getMessage());
			e.printStackTrace();
		} catch (InstantiationException e) {
			log.error(e.getMessage());
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			log.error(e.getMessage());
			e.printStackTrace();
		}
	}

	/**
	 * Scans all classes accessible from the context class loader which belong to the given package and subpackages.
	 *
	 * @param packageName The base package
	 * @return The classes
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	private static List<Class> getClasses(String packageName)
	        throws ClassNotFoundException, IOException 
	{
	    ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
	    assert classLoader != null;
	    String path = packageName.replace('.', '/');
	    Enumeration<URL> resources = classLoader.getResources(path);
	    List<File> dirs = new ArrayList<File>();
	    while (resources.hasMoreElements()) {
	        URL resource = resources.nextElement();
	        String fileName = resource.getFile();
	        String fileNameDecoded = URLDecoder.decode(fileName, "UTF-8");
	        dirs.add(new File(fileNameDecoded));
	    }
	    ArrayList<Class> classes = new ArrayList<Class>();
	    for (File directory : dirs) {
	        classes.addAll(findClasses(directory, packageName));
	    }
	    return classes;
	}

	/**
	 * Recursive method used to find all classes in a given directory and subdirs.
	 *
	 * @param directory   The base directory
	 * @param packageName The package name for classes found inside the base directory
	 * @return The classes
	 * @throws ClassNotFoundException
	 */
	@SuppressWarnings("unchecked")
	private static List<Class> findClasses(File directory, String packageName) throws ClassNotFoundException 
	{
	    List<Class> classes = new ArrayList<Class>();
	    if (!directory.exists()) {
	        return classes;
	    }
	    File[] files = directory.listFiles();
	    for (File file : files) {
	    	String fileName = file.getName();
	        if (file.isDirectory()) {
	            assert !fileName.contains(".");
	        	classes.addAll(findClasses(file, packageName + "." + fileName));
	        } else if (fileName.endsWith(".class") && !fileName.contains("$")) {
	        	Class _class;
				try {
					_class = Class.forName(packageName + '.' + fileName.substring(0, fileName.length() - 6));
				} catch (ExceptionInInitializerError e) {
					// happen, for example, in classes, which depend on 
					// Spring to inject some beans, and which fail, 
					// if dependency is not fulfilled
					_class = Class.forName(packageName + '.' + fileName.substring(0, fileName.length() - 6),
							false, Thread.currentThread().getContextClassLoader());
				}
				classes.add(_class);
	        }
	    }
	    return classes;
	}

}
