package utils;

public enum ScaleType {
	Nominal, Ordinal, Interval, Ratio

}
