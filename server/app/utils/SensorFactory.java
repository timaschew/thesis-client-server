package utils;

import java.util.List;

import play.db.jpa.GenericModel.JPAQuery;
import models.Sensor;


public class SensorFactory {

	/*
	 * Android sensors
	 */
	private final static String[] ANDROID_SENSORS = new String[] {
			"TYPE_ACCELEROMETER"
			, "TYPE_ALL"
			, "TYPE_AMBIENT_TEMPERATURE"
			, "TYPE_GRAVITY"
			, "TYPE_GYROSCOPE"
			, "TYPE_LIGHT"
			, "TYPE_LINEAR_ACCELERATION"
			, "TYPE_MAGNETIC_FIELD"
			, "TYPE_ORIENTATION"
			, "TYPE_PRESSURE"
			, "TYPE_PROXIMITY"
			, "TYPE_RELATIVE_HUMIDITY"
			, "TYPE_ROTATION_VECTOR"
			, "TYPE_TEMPERATURE"};
	
	public static void createSensor() {
		
	}
	
	public static int createAndroidSensors() {
		int i=0;
		for (String s : ANDROID_SENSORS) {
			List<Object> result = Sensor.find("byName", s).fetch();
			// only add if not exist already
			if (result.isEmpty()) {
				Sensor sensor = new Sensor(s);
				sensor.save();
				i++;
			}
		}
		return i;
	}
}
