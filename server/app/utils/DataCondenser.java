package utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import play.Logger;


public class DataCondenser {
		
	private static final Double ZERO = new Double(0.0);

	public Map<String, CondenseDataSet> datas = new HashMap<String, CondenseDataSet>();
	
	public Map<String, ArrayList<Long>> timeList = new HashMap<String, ArrayList<Long>>();

	private ArrayList<Data> rawDataList;
	
	public DataCondenser(ArrayList<Data> rawDataList) {
		this.rawDataList = rawDataList;
	}

	public void clear() {
		datas.clear();
	}
	
	public ArrayList<Data> condense(Integer avgWindowMillis) {
		long startTime = rawDataList.get(0).timestamp;
		ArrayList<Data> resultList = new ArrayList<Data>();

		for (int i=1; i<rawDataList.size(); i++) {
			
			Data currentData = rawDataList.get(i);
			long currentTime = currentData.timestamp;
			long diff = currentTime - startTime;
			if (diff <= avgWindowMillis) {
				// collect for average
				updateSumAndTimestamp(currentData);
			} else {
				// calculate average for one or multiple sensors
				ArrayList<Data> retVal = calcAverage();
				resultList.addAll(retVal);
				clear();
				startTime = currentTime;
			}
		}
		return resultList;
	}
	
	protected ArrayList<Data> calcAverage() {
		
		ArrayList<Data> list = new ArrayList<Data>();
		
		// every sensor type is a entry
		for (Entry<String, CondenseDataSet> e : datas.entrySet()) {
			Data d = new Data();
			String key = e.getKey();
			Long avgTime = calcTime(key);
			Logger.debug("condense: using avg time= %d", avgTime);

			CondenseDataSet val = e.getValue();
			Double[] sum = val.sum;
			Integer count = val.count;
			Double[] avgSum = new Double[sum.length];
			for (int i=0; i<sum.length; i++) {
				if (sum[i].equals(ZERO)) {
					// empty dimensions
					avgSum[i] = null;
				} else {
					avgSum[i] = sum[i] / count;
				}
			}
			d.timestamp = avgTime;
			d.name = key;
			d.values = doublesToStrings(avgSum);
			list.add(d);
		}
		return list;
		
	}



	private String[] doublesToStrings(Double[] avgSum) {
		String[] str = new String[avgSum.length];
		for (int i=0; i<avgSum.length; i++) {
			if (avgSum[i] == null) {
				str[i] = "";
			} else {
				str[i] = avgSum[i].toString();
			}
		}
		return str;
	}

	private Long calcTime(String key) {
		ArrayList<Long> times = timeList.get(key);
		Logger.debug("condense: first time= %d last= %d", times.get(0), times.get(times.size()-1));
		if (times.size() > 2) {
			return times.get(times.size()/2);
		} else {
			return times.get(0);
		}
	}

	protected void updateSumAndTimestamp(Data currentData) {
		String key = currentData.name;
		CondenseDataSet ref = datas.get(key);
		if (ref == null) {
			ref = new CondenseDataSet();
			ref.sum = new Double[currentData.values.length];
			for (int i=0; i<ref.sum.length; i++) {
				ref.sum[i] = 0.0;
			}
			ref.count = 0;
		}
		datas.put(key, ref);
		for (int i=0; i<currentData.values.length; i++) {
			ref.sum[i] += Double.parseDouble(currentData.values[i]);
		}
		ref.count++;
		
		ArrayList<Long> tl = timeList.get(key);
		if (tl == null) {
			tl = new ArrayList<Long>();
		}
		tl.add(currentData.timestamp);
		timeList.put(key, tl);
		
	}
}
