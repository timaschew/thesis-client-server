package controllers;

import java.util.List;

import models.Profile;
import models.Sensor;

public class Sensors extends CRUD {
	
	public static List<Sensor> all() {
		return Sensor.all().fetch();
	}

	public static void allAsXML() {
		List<Sensor> list = all();
		renderXml(list);
	}
	
	public static void allAsJSON() {
		List<Sensor> list = all();
		renderJSON(list);
	}

}
