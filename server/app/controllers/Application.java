package controllers;

import play.*;
import play.data.validation.Required;
import play.mvc.*;
import utils.ApplicationHelper;
import utils.SensorFactory;

import java.util.*;

import models.*;

public class Application extends Controller {


	public static final String FILE_SENSOR_PATH = Play.configuration.getProperty("file.upload.path"); //"C:/Users/thunder/Devel/Android/sensors/server/";

	public static void index() {
		render();
	}

	public static void hello(@Required final String name) {
		if (validation.hasErrors()) {
			flash.error("Oops, please enter your name!");
		}
		render(name);
	}
	
	public static void connection()  {
		renderText("world");
	}
	
	public static void initSensors() {
		int amount = SensorFactory.createAndroidSensors();
		renderText("saved "+amount+" sensors");
	}
	
	public static void initPPImpl() {
		ApplicationHelper.loadPreprocessingImplementations();
	}

}