package controllers;

import org.apache.log4j.Logger;
import org.eclipse.jdt.core.dom.ThisExpression;

import models.PreprocessingJob;
import models.Session;
import play.jobs.Every;
import play.jobs.Job;

/**
 * Daemon for doing the preprocessing job.
 * Can be called directly with .now() if there is no annotation file.
 * Or it will be called periodly and checks if the annotation file is available, then starts the job.
 * 
 * TODO: need lock or synchronize stuff for race conditions?
 * @author awilhelm
 *
 */
@Every("60s")
public class Daemon extends Job {
	
	private final static Logger logger = Logger.getLogger(Daemon.class);
	
	private PreprocessingJob job;

	
	/**
	 * for scheduled execution, if annotaion file is uploaded asyn (later than the sensor csv)
	 * @see play.Invoker.Invocation#before()
	 */
	@Override
	public void before() {
		super.before();
		logger.info("daemon starts...");
		PreprocessingJob p = PreprocessingJob.find("byFinished", false).first();
		if (p != null) {
			logger.info(String.format("job found: %s", p.id));
			Session session = p.dataContainer.session;
			if (session != null) {
				if (session.annotationFile != null) {
					this.job = p;
				} else {
					logger.info("job canceled, because annotationFile still missing");
				}
			} 
		} else {
			logger.info("no job was found");
		}
	}
	
	@Override
	public void doJob() throws Exception {
		super.doJob();
		if (job != null) {
			job.work();
		}
	}

}
