package controllers;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.URL;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import org.apache.log4j.Logger;

import play.jobs.Job;
import play.jobs.OnApplicationStart;
import utils.ApplicationHelper;
import utils.SensorFactory;

@OnApplicationStart(async=true)
public class Bootstrap extends Job {
    
	private final Logger logger = Logger.getLogger(this.getClass());
	
	@Override
	public void doJob() throws Exception {
		super.doJob();
		InetAddress ownIP = InetAddress.getLocalHost();
		logger.info("server starts with IP: "+ownIP.getHostAddress());
		 
		loadSensors();
		ApplicationHelper.loadPreprocessingImplementations();
	}
	
	//Logger.info("A log message");
	
    private void loadSensors() {
    	int amount = SensorFactory.createAndroidSensors();
    	logger.info(String.format("loaded %d sensors", amount));
		
    }
    

    
    public void loadLearningAlgorithmImplementations() {
    	logger.info("not yet implemented!");
    }
    

}