package controllers;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.apache.log4j.Logger;

import play.data.validation.Required;
import play.db.jpa.GenericModel.JPAQuery;
import utils.CsvHelper;
import models.Profile;
import models.Session;
import models.User;

public class Sessions extends CRUD {

	private final static Logger log = Logger.getLogger(Sessions.class);

	public static void join() {
		render("Sessions/join.html");
	}
	
	public static void create(String profile, String username) {
		notFoundIfNull(profile, "profile was null");
		notFoundIfNull(username, "username was null");
		Profile p = Profiles.findByName(profile);
		User u = Users.getUserByName(username);
		
		if (p == null) {
			return;
		}
		if (u == null) {
			return;
		}
		Session session = new Session(p, u);
		session.save();
		renderJSON(session);
		
	}
	
	public static void connect(String session) {
		notFoundIfNull(session, "session was null");

		Session foundSession = Session.find("byAnnotationsesssion", session).first();
		if (foundSession == null) {
			renderText("session not found");
		}
		if (foundSession.closed == true) {
			renderText("session alread closed");
		}

		foundSession.remoteIp = request.remoteAddress;
		foundSession.save();
		
		renderArgs.put("annotationSession", foundSession);
		renderArgs.put("profile", foundSession.profile);
		renderArgs.put("user", foundSession.user);
		
		render("Sessions/annotation.html");
		
	}

	
	public static void save(String aresult, String longHash) {
		notFoundIfNull(aresult, "aresult was null");
		notFoundIfNull(longHash, "longHash was null");

		Session session = Session.find("byLonghash", longHash).first();
		if (session == null) {
			renderText("session not found for hash: "+longHash);
			return;
		}
		if (session.closed) {
			renderText("session is already closed");
		}
		
		File file = null;
		try {
			file = File.createTempFile("session", ".csv");
			FileWriter fw = new FileWriter(file);
			fw.write(aresult);
			fw.flush();
			fw.close();
		} catch (IOException e) {
			log.error(e.getMessage());
			return;
		}
		
		
		
		File dir = new File(Application.FILE_SENSOR_PATH);
		dir = new File(dir, session.user.name+"/"+session.profile.name);
		dir.mkdirs();
		
		
		// Move file to new directory
		File movedFile = new File(dir, "session_"+longHash+".csv");
		boolean fileMoveReturnValue = file.renameTo(movedFile);
		if (fileMoveReturnValue == false) {
			log.error(String.format("file could not be moved to %s", movedFile));
			return;
		}

		try {
			session.annotationFile = movedFile.getAbsolutePath();
			session.closed = true;
			session.save();		
			log.info(String.format("saved annotation in file: %s", aresult));
		} catch (Exception e) {
			log.error(String.format("could not update session for annotaion with id %d", session.id));
		}
		
		renderText("upload was successfull");
		
	}
}
