package controllers;

import java.util.List;

import models.DataContainer;

import org.apache.commons.lang.Validate;

import play.data.validation.Required;
import play.mvc.Http.StatusCode;

public class DataContainers extends CRUD {
	
	public static void id(@Required final Long id) {
		DataContainer result = DataContainer.findById(id);
		if (result != null) {
			renderJSON(result);
		}
		response.status = StatusCode.NO_RESPONSE;
	}

}
