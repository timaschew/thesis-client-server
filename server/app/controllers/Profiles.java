package controllers;

import java.io.StringWriter;
import java.io.Writer;
import java.util.List;

import org.apache.commons.lang.Validate;
import org.eclipse.jdt.core.dom.SuperConstructorInvocation;

import play.data.validation.Required;
import play.mvc.Http.StatusCode;
import play.utils.HTTP;

import models.Profile;
import models.Sensor;

public class Profiles extends CRUD {
	
	public static List<Profile> all() {
		return Profile.all().fetch();
	}

	public static void allAsXML() {
		List<Profile> list = all();
		renderXml(list);
	}
	
	public static void allAsJSON() {
		List<Profile> list = all();
		renderJSON(list);
	}
	
	public static Profile findByName(String profileName) {
		notFoundIfNull(profileName, "profile was null");
		List<Profile> result = Profile.find("byName", profileName).fetch();
		if (result.isEmpty() == false) {
			return result.get(0);
		}
		return null;
	}
	
	
	public static void name(final String name) {
		notFoundIfNull(name, "name was null");
		List<Profile> result = Profile.find("byName", name).fetch();
		Validate.isTrue(result.size() <= 1, "found multiple results for unique profile name");
		if (result.size() == 1) {
			renderJSON(result.get(0));
		}
		response.status = StatusCode.NO_RESPONSE;
	}


}
