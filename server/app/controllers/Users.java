package controllers;

import java.util.List;

import play.db.jpa.GenericModel.JPAQuery;

import models.Profile;
import models.User;

public class Users extends CRUD {

	
	public static boolean checkPassForUser(String username, String password) {
		notFoundIfNull(username, "username was null");
		notFoundIfNull(password, "password was null");
		User user = getUserByName(username);
		if (user.checkPassword(password)) {
			return true;
		} 
		return false;	
	}
	
	public static User getUserByName(String username) {
		notFoundIfNull(username, "username was null");
		List<User> users = User.find("byName", username).fetch();
		if (users.isEmpty() == false) {
			return users.get(0);
		}
		return null;
	}
	
}
