package controllers;

import java.io.File;
import java.util.Date;

import org.apache.log4j.Logger;

import models.DataContainer;
import models.PreprocessingJob;
import models.Profile;
import models.Session;
import models.User;

import play.mvc.Controller;

public class FileUpload extends Controller {
	
	private final static Logger log = Logger.getLogger(FileUpload.class);
	
	public static void uploadPath() {
		renderText(Application.FILE_SENSOR_PATH);
	}
	
	public static void pseudoUpload() {
		DataContainer dc = new DataContainer();
		dc.fileName = "test";
		dc.profile = Profile.findById(18L);
		dc.user = User.findById(17L);
		dc.recordDate = new Date().getTime();
		dc.save();
		renderText(dc.id.toString());
	}

	public static void uploadFile(final File file, String username, String password, String profileName, String annotationID) {
		notFoundIfNull(file, "file was null");
		notFoundIfNull(username, "username was null");
		notFoundIfNull(password, "password was null");
		notFoundIfNull(profileName, "profileName was null");
		notFoundIfNull(annotationID, "annotationID was null");

		
		log.info(String.format("file upload ACK with file %s", file));
		log.info(String.format("user: %s", username));
		User user = Users.getUserByName(username);
		
		boolean isAuth = user.checkPassword(password);
		log.info(String.format(String.format("user auth: %s", isAuth)));
		log.info(String.format("file location: %s", file.getAbsoluteFile()));
		
		Session session = Session.findById(Long.valueOf(annotationID));
	

		DataContainer dc = new DataContainer();
		dc.profile = Profiles.findByName(profileName);
		dc.session = session;
		// Destination directory
		File dir = new File(Application.FILE_SENSOR_PATH);
		dir = new File(dir, username+"/"+dc.profile.name);
		dir.mkdirs();
		
		
		// Move file to new directory
		File movedFile = new File(dir, file.getName());
		boolean fileMoveReturnValue = file.renameTo(movedFile);
		if (fileMoveReturnValue == false) {
			log.error(String.format("file could not be moved to %s", movedFile));
			return;
		}
		
		
		dc.fileName = movedFile.getAbsolutePath();
		dc.user = user;
		dc.recordDate = new Date().getTime();
		
		if (dc.profile.editable) {
			dc.profile.editable = false;
			dc.profile.save();
		}
		
		dc = dc.save();
		
		PreprocessingJob job = new PreprocessingJob(dc);
		job.save();
		
		if (session == null) {
			Daemon daemon = new Daemon();
			daemon.now(); // call deamon now
		} 
		
		
		
		renderText(dc.id.toString());
		
	}

}
