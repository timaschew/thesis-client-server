package models;

import javax.persistence.Entity;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
abstract public class NamedDescriptionModel extends NamedModel  {
	public String description;
}
