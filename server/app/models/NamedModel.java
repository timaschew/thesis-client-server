package models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.MappedSuperclass;

import play.data.validation.Required;
import play.db.jpa.Model;

@MappedSuperclass
abstract public class NamedModel extends Model {
	@Column(nullable=false, unique=true)
	@Required
	public String name;
	
	@Override
	public String toString() {
		return name;
	}

}
