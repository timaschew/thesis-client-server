package models;

import impl.AbstractPreprocessingImpl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.log4j.Logger;

import controllers.Application;

import play.db.jpa.Model;
import play.jobs.Every;
import play.jobs.Job;
import utils.CsvHelper;
import utils.Data;

@Entity
@Table(name="pp_job")
public class PreprocessingJob extends Model {
	
	@Transient
	private final Logger log = Logger.getLogger(this.getClass().getName());

	@ManyToOne(cascade = CascadeType.ALL, optional = false)
	public DataContainer dataContainer;
	
	public boolean finished = false;

	public PreprocessingJob(DataContainer dataContainer) {
		this.dataContainer = dataContainer;
	}

	public void work() {
		
		ArrayList<Data> dataList = CsvHelper.parseCsv(dataContainer.fileName);
//		try {
//			dataContainer = DataContainer.findById(dataContainer.id);
//		} catch (Exception e) {
//			log.error(String.format("could not load datacontainer with id %d", dataContainer.id));
//		}
		dataContainer.dataSize = dataList.size();
		if (dataList.size() > 0) {
			long start = dataList.get(0).timestamp;
			long end = dataList.get(dataList.size()-1).timestamp;
			dataContainer.recordDate = start;
			long duration = end - start;
			dataContainer.recordDuration = duration;
		}
		dataContainer.save();
		
		Preprocessing p = dataContainer.profile.preprocessing;
		Preprocessing pImpl = Preprocessing.find("byName", p.name).first();
		if (pImpl != null) {
			
			Class<?> clazz = null;
			AbstractPreprocessingImpl ppImpl = null;
			try {
				clazz = Class.forName(pImpl.name);
				ppImpl = (AbstractPreprocessingImpl) clazz.newInstance();
			} catch (Exception e) {
				log.error(e.getMessage());
			} 
			checkForAnnotation(dataList);
			ppImpl.process(dataList);
			dataContainer.result = ppImpl.doAfter();
			dataContainer.save();
			finished = true;
			save();
		} else {
			log.error(String.format("preprocessing impl not found: %s", p.name));
		}
	
	}

	private void checkForAnnotation(ArrayList<Data> sensorDataList) {
		
		ArrayList<Data> annotationDataList = CsvHelper.parseAnnotationCsv(dataContainer.session.annotationFile);
		Iterator<Data> aIt = annotationDataList.iterator();
		
		if (aIt.hasNext() == false) {
			log.warn(String.format("annotation datas are emtpy in file %s", dataContainer.session.annotationFile));
			return;
		}
		Data aData = aIt.next();
		Data aPrev = aData;
		for (Data sensorData : sensorDataList) {
			// no annotation available, skip this data
			if (sensorData.timestamp < aPrev.timestamp) {
				continue;
			}
			
			if (sensorData.timestamp >= aPrev.timestamp) {
				// add prev
				sensorData.meta = aPrev.meta;
			}
			
			while (sensorData.timestamp >= aData.timestamp) {
				if (aIt.hasNext() == false) {
					aPrev = aData;
					break;
				}
				aPrev = aData;
				aData = aIt.next();
			}			
		}
		
		Session session = dataContainer.session;
		String longHash = session.longHash;
		
		File dir = new File(Application.FILE_SENSOR_PATH);
		dir = new File(dir, session.user.name+"/"+session.profile.name);
		dir.mkdirs();
		
		
		// Move file to new directory
		File file = new File(dir, "sensor_with_annotation_session_"+longHash+".csv");
		CsvHelper.write(sensorDataList, file, false);
		
	}

}
