package models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import play.data.validation.Required;
import play.db.jpa.Model;
import utils.ScaleType;

@Entity
public class Sensor extends Model {
	
	@Required
	@Column(unique=true) 
	public String name;
	@Enumerated(EnumType.STRING)
	public ScaleType scale;
	@Enumerated(EnumType.STRING)
	public SensorType type;
	public Class valueType;
	
	public Sensor(final String name) {
		this(name, SensorType.Mobile, ScaleType.Ratio, Float.class);
	}
	
	
	public Sensor(final String name, final SensorType type, final ScaleType scale, final Class valueType) {
		this.name = name;
		this.type = type;
		this.scale = scale;
		this.valueType = valueType;
	}

	@Override
	public String toString() {
		return name;
	}

}
