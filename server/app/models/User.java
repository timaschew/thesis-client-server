package models;

import javax.persistence.Entity;
import javax.persistence.Table;

import play.data.validation.Password;
import play.data.validation.Required;
import play.libs.Codec;

/**
 * User model with name and password.
 * Description inherits from super class and is optinal.
 * 
 * @author awilhelm
 *
 */
@Entity
@Table(name="t_user")
public class User extends NamedDescriptionModel {
	
	/**
	 * password, which will be saved in hash form in database
	 */
	@Required
	@Password
	public String password;
	
	public boolean checkPassword(String pass) {
		return password.equals(Codec.hexMD5(pass));
	}
	
	public void setPassword(String password) {
		this.password = Codec.hexMD5(password);
	}
	
	
	
}
