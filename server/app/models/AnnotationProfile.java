package models;

import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.OneToMany;

public class AnnotationProfile extends Profile {

	public AnnotationProfile() {
		learnType = LearnType.SUPERVISED;
	}
	
	@OneToMany(cascade=CascadeType.ALL)
	public Set<String> annotations;
	
}
