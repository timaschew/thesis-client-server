package models;

import java.io.StringWriter;
import java.io.Writer;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import net.sf.oval.guard.Pre;

import org.hibernate.annotations.IndexColumn;

import play.data.validation.Required;
import play.db.jpa.Model;

@Entity
@Table(name="profile")
public class Profile extends NamedDescriptionModel {
	
	@ManyToMany(cascade=CascadeType.ALL)
	public List<Sensor> sensor;
	
	@OneToOne(cascade=CascadeType.ALL, optional = false)
	public Preprocessing preprocessing;
	
	@Enumerated(EnumType.STRING)
	public LearnType learnType;
	
	@Enumerated(EnumType.STRING)
	public SamplingRate delay;
	
	@Column(nullable=false)
	public Boolean editable = true;
	
	public String annotations;
	
	public List<String> getAnnotationsAsList() {
		if (annotations == null) {
			return Collections.EMPTY_LIST;
		}
	    return Arrays.asList(annotations.split(";"));
	}
}
