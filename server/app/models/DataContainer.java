package models;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import play.db.jpa.Model;
import utils.Data;

@Entity
@Table(name="datacontainer")
public class DataContainer extends Model {
	
	private final static DecimalFormat FMT = new DecimalFormat("#.##", new DecimalFormatSymbols(Locale.getDefault()));
	
	@ManyToOne(cascade = CascadeType.ALL, optional = false)
	public User user;
	
	@ManyToOne(cascade = CascadeType.ALL, optional = false)
	public Profile profile;
	
	@ManyToOne(cascade = CascadeType.ALL, optional = true)
	public Session session;
		
	public String result;
	
	@Column(nullable = false)
	public Long recordDate;
	
	public Long recordDuration = -1L;
	
	@Column(nullable = false)
	public String fileName;
	
	public Integer dataSize;
	
	@Transient
	public ArrayList<Data> data;
	
	@Override
	public String toString() {
		double durationInMinutes = (((double)recordDuration)/1000)/60;
		
		StringBuilder sb = new StringBuilder();
		sb.append(super.toString()).append(";");
		sb.append(user.name).append(";");
		sb.append(profile.name).append(";");
		sb.append(dataSize).append(";");
		sb.append(FMT.format(durationInMinutes)).append("min;");
		sb.append(fileName);
		
		return sb.toString();
	}
}
