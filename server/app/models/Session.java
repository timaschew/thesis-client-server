package models;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import play.db.jpa.Model;
import play.libs.Codec;

/**
 * 
 * 
 * @author awilhelm
 *
 */
@Entity
@Table(name="annotation_session")
public class Session extends Model {
	
	public Session(Profile profile, User user) {
		this.profile = profile;
		this.user = user;
		createHash();
	}

	@Column(nullable = false)
	public String annotationsesssion;
	
	@Column(nullable = false)
	public String longHash;
	
	@ManyToOne(cascade = CascadeType.ALL, optional = false)
	public Profile profile;
	
	@ManyToOne(cascade = CascadeType.ALL, optional = false)
	public User user;
	
	@Column(nullable = true)
	public String annotationFile;
	
	public String remoteIp;
		
	public boolean closed = false;

	private void createHash() {
		longHash = Codec.hexSHA1(profile.name+user.name+new Date().toString());
		annotationsesssion = longHash.substring(0, 6);
	}
	
	

}
