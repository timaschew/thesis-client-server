package impl.preprocessing;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.Map.Entry;
import java.util.Vector;

import org.apache.commons.collections.map.HashedMap;
import org.apache.log4j.Logger;

import utils.CsvHelper;
import utils.Data;
import utils.DataCondenser;
import utils.DataHelper;
import utils.RealData;
import utils.WekaHelper;
import weka.clusterers.ClusterEvaluation;
import weka.clusterers.EM;
import weka.core.Instances;

import models.Preprocessing;
import impl.AbstractPreprocessingImpl;
import impl.PreprocessingInterface;

/**
 * 
 * @author awilhelm
 *
 */
public class MyCustomPreprocessing extends AbstractPreprocessingImpl {
	
	private final Logger log = Logger.getLogger(this.getClass().getName());

	
	public static final Integer AVG_WINDOW_MILLIS = 100;
	
//	public static final String[] VECTOR = new String[] {"TYPE_ORIENTATION", "TYPE_GYROSCOPE", "TYPE_ACCELEROMETER"};
	
	public static final String[] VECTOR = new String[] {"TYPE_ACCELEROMETER"};

	
	@Override
	public void doBefore() {
		super.doBefore();
		preprocessing.description="non sense";
	}
	
	@Override
	public String doAfter() {
		return super.doAfter();
	}
	
	@Override
	public void process(ArrayList<Data> rawDataList) {
		
		log.info(String.format("raw data: %s", rawDataList.size()));
		
		DataCondenser dc = new DataCondenser(rawDataList);
		ArrayList<Data> resultList = dc.condense(AVG_WINDOW_MILLIS);
		
		log.info(String.format("condensed data: %s", resultList.size()));
				
		ArrayList<ArrayList<Data>> listOfVectors = DataHelper.createPseudoInputVectors(resultList, VECTOR);
		
	    
		
	}

	


}
