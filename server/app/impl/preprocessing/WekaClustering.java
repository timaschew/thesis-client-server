package impl.preprocessing;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.Map.Entry;
import java.util.Vector;

import org.apache.commons.collections.map.HashedMap;
import org.apache.log4j.Logger;

import controllers.Sessions;

import utils.CsvHelper;
import utils.Data;
import utils.DataCondenser;
import utils.DataHelper;
import utils.RealData;
import utils.WekaHelper;
import weka.clusterers.ClusterEvaluation;
import weka.clusterers.EM;
import weka.core.Instances;

import models.Preprocessing;
import impl.AbstractPreprocessingImpl;
import impl.PreprocessingInterface;

/**
 * dependency: weka (3.6.6-strable)
 * 
 * @author awilhelm
 *
 */
public class WekaClustering extends AbstractPreprocessingImpl {
	
	private final Logger log = Logger.getLogger(this.getClass().getName());
	
	public static final Integer AVG_WINDOW_MILLIS = 100;
	
	public static final String[] VECTOR = new String[] {"TYPE_ORIENTATION", "TYPE_GYROSCOPE", "TYPE_ACCELEROMETER"};
	
//	public static final String[] VECTOR = new String[] {"TYPE_ACCELEROMETER"};

private int amountOfClusters;

	
	@Override
	public void doBefore() {
		super.doBefore();
		preprocessing.description="clustering unsupervised data with weka";
	}
	
	@Override
	public String doAfter() {
		super.doAfter();
		return Integer.valueOf(amountOfClusters).toString();
	}
	
	@Override
	public void process(ArrayList<Data> rawDataList) {
		
		log.info(String.format("raw data: %s", rawDataList.size()));
		
		DataCondenser dc = new DataCondenser(rawDataList);
		ArrayList<Data> resultList = dc.condense(AVG_WINDOW_MILLIS);
		
		
		log.info(String.format("condensed data: %s", resultList.size()));
				
		ArrayList<ArrayList<RealData>> listOfVectors = DataHelper.createInputVectors(resultList, VECTOR);
		
		Instances arffData = WekaHelper.createArffFile(listOfVectors, "test");
		
		log.debug(String.format("arff file\n%s", arffData));
		
		EM cl = new EM();
	    try {
			cl.buildClusterer(arffData);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	    ClusterEvaluation eval = new ClusterEvaluation();
	    eval.setClusterer(cl);
	    try {
			eval.evaluateClusterer(new Instances(arffData));
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		log.debug(String.format("cluster result\n%s", eval.clusterResultsToString()));
	    amountOfClusters = eval.getNumClusters();
		log.debug(String.format("amount of clusters %s", amountOfClusters));

//	    // cross-validation for density based clusterers
//	    // NB: use MakeDensityBasedClusterer to turn any non-density clusterer
//	    //     into such.
//	    System.out.println("\n--> Cross-validation");
//	    cl = new EM();
//	    double logLikelyhood = Double.MAX_VALUE;
//		try {
//			logLikelyhood = ClusterEvaluation.crossValidateModel(
//			       cl, arffData, 10, arffData.getRandomNumberGenerator(1));
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	    System.out.println("log-likelyhood: " + logLikelyhood);
	    
		
	}

	


}
