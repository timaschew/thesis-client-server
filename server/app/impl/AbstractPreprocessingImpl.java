package impl;

import models.Preprocessing;

abstract public class AbstractPreprocessingImpl implements PreprocessingInterface {
	
	public Preprocessing preprocessing;
	public boolean finalized;
	
	public AbstractPreprocessingImpl() {
		preprocessing = new Preprocessing();
	}
	
	@Override
	public void doBefore() {
		preprocessing.name=this.getClass().getName();
	}
	
	@Override
	public String doAfter() {
		finalized = true;
		return "return value from abstract class";
	}
	
}
