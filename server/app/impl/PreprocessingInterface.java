package impl;

import java.util.ArrayList;

import javax.persistence.Transient;

import utils.Data;


public interface PreprocessingInterface {
	
	@Transient
	void doBefore();
	
	@Transient
	String doAfter();
	
	@Transient
	void process(ArrayList<Data> rawDataList);
	
}
